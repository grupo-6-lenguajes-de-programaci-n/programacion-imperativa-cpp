#include <iostream>
#include <chrono>

int main() {
    int n;

    // Solicitar al usuario el número para calcular el factorial
    std::cout << "Ingrese un número entero positivo: ";
    std::cin >> n;

    // Validar que el número sea positivo
    if (n < 0) {
        std::cout << "El número debe ser positivo." << std::endl;
        return 1; // Salir con código de error
    }

    // Iniciar el cronómetro
    auto start = std::chrono::high_resolution_clock::now();

    long long factorial = 1; // Usamos long long para manejar factoriales grandes

    // Calcular el factorial utilizando un bucle for
    for (int i = 1; i <= n; ++i) {
        factorial *= i; // Multiplicar factorial por el valor actual de i
    }

    // Detener el cronómetro
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> duration = end - start;

    // Mostrar el resultado y el tiempo de ejecución
    std::cout << "El factorial de " << n << " es " << factorial << std::endl;
    std::cout << "Tiempo de ejecución: " << duration.count() << " segundos" << std::endl;

    return 0; // Salir con éxito
}